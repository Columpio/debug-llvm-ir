## Usage

```bash
./debug_llvm_ir.py /full/path/to/ll/file.ll /full/path/to/json/file.json > assembly.ll
```

will give you all functions from the first trace in json file.

```bash
./debug_llvm_ir.py /full/path/to/ll/file.ll /full/path/to/json/file.json 42 > assembly.ll
```

will give you all functions from the trace with `"id": 42`.

## Note

Script also adds "`i # `" prefixes before instruction meaning "i-th event in a trace touches this instruction".
