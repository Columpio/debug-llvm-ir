#!/usr/bin/env python3
import sys
import re
import json
from collections import defaultdict

def readFunctions(file, queue):
  codes = dict()
  while queue:
    found = False
    lines = []
    current = None
    pattern = f"define .* @(?P<name>{'|'.join(queue)})\("
    for line in file:
      if found:
        lines.append(line)
        if line.strip() == "}":
          break
        continue
      m = re.search(pattern, line)
      if m:
        current = m.group("name")
        queue.remove(current)
        found = True
        lines.append(line)
    codes[current] = lines
  return codes

def enumerateFunctionLines(lines, debug_nums):
  i = 0
  numbered_lines = []
  for line in lines:
    out_line = ""
    num = None
    if re.match(r"^  [^\s\]]", line):
      m = re.fullmatch("^(?P<command>.*), !dbg !(?P<num>\d+)\s*$", line)
      if m:
        num = int(m.group("num"))
        line = m.group("command")
        debug_nums.add(num)
      out_line += f"{i}:"
      i += 1
    out_line += line
    numbered_lines.append((out_line, num))
  return numbered_lines

def getDebugInfos(file, debug_nums):
  debug_infos = {}
  def handleInfo(num, line):
    _, _, info = line.partition('DILocation')
    nonlocal debug_infos
    debug_infos[num] = info
  if not debug_nums:
    return debug_infos
  debug_nums = list(debug_nums)
  debug_nums.sort(reverse=True)
  for line in file:
    if line.startswith("!0 = "):
      if debug_nums[-1] == 0:
        handleInfo(0, line)
        debug_nums.pop()
      break
  cur = 1
  while debug_nums:
    last = debug_nums[-1]
    debug_nums.pop()
    for line in file:
      if cur == last:
        handleInfo(last, line)
        cur += 1
        break
      cur += 1
  return debug_infos

class Code:
  def __init__(self):
    pass

def extract_functions(filename, trace):
  with open(filename) as file:
    codes = readFunctions(file, set(trace.keys()))
    debug_nums = set()
    numbered_codes = dict()
    for function, code in codes.items():
      numbered_lines = enumerateFunctionLines(code, debug_nums)
      numbered_codes[function] = numbered_lines
    debug_infos = getDebugInfos(file, debug_nums)
  for function, numbered_lines in numbered_codes.items():
    events = trace[function]
    i = 0
    event = events[i]
    offset = f"{event.offset}:"
    for line, num in numbered_lines:
      if i is not None and line.startswith(offset):
        print(f"{event.i} # ", end='')
        i += 1
        if i == len(events):
          i = None
        else:
          event = events[i]
          offset = f"{event.offset}:"
      print(line, end='')
      if num != None:
        print(',', debug_infos[num], end='')

class Event:
  def __init__(self, i, location, event):
    self.i = i
    self.offset = location["instructionOffset"]
    self.event = event
  
  def __repr__(self):
    return f"#{self.i}: at {self.offset}"

def parse_trace(json_name, trace_id):
  with open(json_name) as file:
    traces = json.load(file)["errors"]
  if trace_id is None:
    trace = traces[0]
  else:
    for trace in traces:
      if trace['id'] == trace_id:
        break
  traced = defaultdict(list)
  for i, e in enumerate(trace['trace'], start=1):
    location = e["location"]
    traced[location["function"]].append(Event(i, location, e["event"]))
  return traced

def main():
  filename = sys.argv[1]
  json_name = sys.argv[2]
  trace_id = int(sys.argv[3]) if len(sys.argv) > 3 else None
  trace = parse_trace(json_name, trace_id)
  extract_functions(filename, trace)


if __name__ == "__main__":
  main()
